new Vue({
  el: '#app',
  data: {
    playerHealth: 100,
    monsterHealth: 100,
    gameIsRunning: false,
    turns: [],
  },
  methods: {
    startGame: function () {
      this.turns = [];
      this.gameIsRunning = true;
      this.playerHealth = 100;
      this.monsterHealth = 100;
    },
    attack: function () {
      let damage = this.calculateDamage(3, 10);
      this.monsterHealth -= damage;
      this.turns.unshift({
        isPlayer: true,
        text: 'Player hits for ' + damage,
      });
      if (this.checkWin()) {
        return;
      }
      this.monsterAttacks();
    },
    specialAttack: function () {
      const damage = this.calculateDamage(10, 20);
      this.monsterHealth -= damage;
      this.turns.unshift({
        isPlayer: true,
        text: 'Player hits Hard for ' + damage,
      });
      if (this.checkWin()) {
        return;
      }
      this.monsterAttacks();
    },
    heal: function () {
      if (this.playerHealth <= 90) {
        this.playerHealth += 10;
      } else {
        this.playerHealth = 100;
      }
      this.turns.unshift({
        isPlayer: true,
        text: 'Player heals for 10',
      });
      this.monsterAttacks();
    },
    giveUp: function () {
      this.gameIsRunning = false;
      this.turns = [];
    },
    monsterAttacks: function () {
      let damage = this.calculateDamage(5, 12);
      this.playerHealth -= damage;
      this.checkWin();
      this.turns.unshift({
        isPlayer: false,
        text: 'Monster hits player for ' + damage,
      });
    },
    calculateDamage: function (min, max) {
      return Math.max(Math.floor(Math.random() * max) + 1, min);
    },
    checkWin: function () {
      if (this.monsterHealth <= 0) {
        this.endOfGameAlert(true);
      } else if (this.playerHealth <= 0) {
        this.endOfGameAlert(false);
      }
      return false;
    },

    endOfGameAlert: function (playerWins) {
      if (playerWins) {
        if (confirm('You Won! New Game?')) {
          this.startGame();
        } else {
          this.monsterHealth = 0;
          this.gameIsRunning = false;
        }
        return true;
      } else {
        if (confirm('You Lost! New Game?')) {
          this.startGame();
        } else {
          this.playerHealth = 0;
          this.gameIsRunning = false;
        }
        return true;
      }
    },
  },
});
